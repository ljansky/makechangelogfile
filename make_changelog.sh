#!/bin/bash
echo "CREATE DATABASE IF NOT EXISTS current; DROP DATABASE IF EXISTS latest; CREATE DATABASE latest; USE latest;" > /tmp/init.sql
cat /tmp/db/latest.sql >> /tmp/init.sql

service mysql start && mysql < /tmp/init.sql

liquibase --changeLogFile=/tmp/changelog.xml migrate

counter=`ls -l /tmp/db/changelogs/ | grep -v ^l | wc -l`

liquibase diffChangeLog | sed 's/TIMESTAMP(19)/TIMESTAMP/g' | sed 's/type=\"BIT\"/type=\"TINYINT(1)\"/g' > /tmp/db/changelogs/$(echo $counter).xml