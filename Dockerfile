FROM ubuntu:16.04

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server wget default-jre

RUN apt-get -y install libmysql-java

RUN mkdir /opt/liquibase
WORKDIR /opt/liquibase
RUN wget https://github.com/liquibase/liquibase/releases/download/liquibase-parent-3.5.3/liquibase-3.5.3-bin.tar.gz
RUN tar zxvf liquibase-3.5.3-bin.tar.gz

RUN ln -s /opt/liquibase/liquibase /usr/local/bin/liquibase

COPY make_changelog.sh /tmp/make_changelog.sh
COPY liquibase.properties /tmp/liquibase.properties
COPY changelog.xml /tmp/changelog.xml

RUN chmod 777 /tmp/make_changelog.sh

RUN echo "lower_case_table_names = 1" >> /etc/mysql/mysql.conf.d/mysqld.cnf

WORKDIR /tmp

EXPOSE 3306

ENTRYPOINT [ "/tmp/make_changelog.sh" ]